
#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <utility>
#include <cmath>
#include "cuda_runtime.h"
#include "device_launch_parameters.h"
#include <thrust/device_vector.h>
#include <thrust/host_vector.h>
#include <thrust/pair.h>
#include <thrust/extrema.h>
#include <thrust/copy.h>
#include "d2hex.h"
#include "segment.h"
#include "gridcell.h"
//#include "assignCell.h"
#include "indexedPoint.h"
//#include "breakSegments.h"
//#include "rayshooter.h"
//#include "utilFunctions.h"
//#include "printFunctions.h"
#include "readData.h"

#include <stdio.h>


int main()
{

	const int NUM_OF_FILES = 2;     // Constant for 2 regions

	const int cellSize = 2;         // Manually set for testing
									//const bool debugPrint = true;   // Used for setting output

									//input files
	std::string files[NUM_OF_FILES] = { "dbg1.txt", "dbg2.txt" };

	int totalLineCount = 0;
	int xOffset, yOffset;
	int colCount, rowCount;
	double xMax;
	double xMin;
	double yMax;
	double yMin;


	//int hashedSegments[][];
	//int hashedRegion[NUM_OF_FILES];

	// Set of cells that have multiple segments passing through them
	//unordered_set<gridcell> multipleLineSet;
	//unordered_set<gridcell> multipleLineSetRegion[NUM_OF_FILES];
	typedef thrust::tuple<int, int> gridCell;
	thrust::host_vector<gridCell> h_multipleLineSet;
	thrust::host_vector<gridCell> h_multipleLineSetPerRegion[NUM_OF_FILES];


	//std::vector<indexedPoint> pointsOfIntersection;
	indexedPoints pointsOfIntersection;

	segments finalSegments[NUM_OF_FILES];

	//std::vector<segment> segmentFromRegions[NUM_OF_FILES];
	segments segmentsFromRegions[NUM_OF_FILES];

	cout << "About to read." << endl;

	/* File input */
	for (int i = 0; i < NUM_OF_FILES; i++)
	{
		readSegmentsFromOdyssey(i, files[i], segmentsFromRegions[i]);
	}

	cout << " Just read." << endl;


	auto result = thrust::minmax_element(segmentsFromRegions[0].dx.begin(), segmentsFromRegions[0].dx.end());
	xMin = segmentsFromRegions[0].dx[result.first - segmentsFromRegions[0].dx.begin()];
	xMax = segmentsFromRegions[0].dx[result.second - segmentsFromRegions[0].dx.begin()];
	result = thrust::minmax_element(segmentsFromRegions[0].sx.begin(), segmentsFromRegions[0].sx.end());
	if (xMin > segmentsFromRegions[0].sx[result.first - segmentsFromRegions[0].sx.begin()])
		xMin = segmentsFromRegions[0].sx[result.first - segmentsFromRegions[0].sx.begin()];
	if (xMax < segmentsFromRegions[0].sx[result.second - segmentsFromRegions[0].sx.begin()])
		xMax = segmentsFromRegions[0].sx[result.second - segmentsFromRegions[0].sx.begin()];

	result = thrust::minmax_element(segmentsFromRegions[1].dx.begin(), segmentsFromRegions[1].dx.end());
	xMin = segmentsFromRegions[1].dx[result.first - segmentsFromRegions[1].dx.begin()];
	xMax = segmentsFromRegions[1].dx[result.second - segmentsFromRegions[1].dx.begin()];
	result = thrust::minmax_element(segmentsFromRegions[1].sx.begin(), segmentsFromRegions[1].sx.end());
	if (xMin > segmentsFromRegions[1].sx[result.first - segmentsFromRegions[1].sx.begin()])
		xMin = segmentsFromRegions[1].sx[result.first - segmentsFromRegions[1].sx.begin()];
	if (xMax < segmentsFromRegions[1].sx[result.second - segmentsFromRegions[1].sx.begin()])
		xMax = segmentsFromRegions[1].sx[result.second - segmentsFromRegions[1].sx.begin()];


	result = thrust::minmax_element(segmentsFromRegions[0].dy.begin(), segmentsFromRegions[0].dy.end());
	yMin = segmentsFromRegions[0].dy[result.first - segmentsFromRegions[0].dy.begin()];
	yMax = segmentsFromRegions[0].dy[result.second - segmentsFromRegions[0].dy.begin()];

	result = thrust::minmax_element(segmentsFromRegions[0].sy.begin(), segmentsFromRegions[0].sy.end());
	if (yMin > segmentsFromRegions[0].sy[result.first - segmentsFromRegions[0].sy.begin()])
		yMin = segmentsFromRegions[0].sy[result.first - segmentsFromRegions[0].sy.begin()];
	if (yMax < segmentsFromRegions[0].sy[result.second - segmentsFromRegions[0].sy.begin()])
		yMax = segmentsFromRegions[0].sy[result.second - segmentsFromRegions[0].sy.begin()];

	result = thrust::minmax_element(segmentsFromRegions[1].dy.begin(), segmentsFromRegions[1].dy.end());
	yMin = segmentsFromRegions[1].dy[result.first - segmentsFromRegions[1].dy.begin()];
	yMax = segmentsFromRegions[1].dy[result.second - segmentsFromRegions[1].dy.begin()];
	result = thrust::minmax_element(segmentsFromRegions[1].sy.begin(), segmentsFromRegions[1].sy.end());
	if (yMin > segmentsFromRegions[1].sy[result.first - segmentsFromRegions[1].sy.begin()])
		yMin = segmentsFromRegions[1].sy[result.first - segmentsFromRegions[1].sy.begin()];
	if (yMax < segmentsFromRegions[1].sy[result.second - segmentsFromRegions[1].sy.begin()])
		yMax = segmentsFromRegions[1].sy[result.second - segmentsFromRegions[1].sy.begin()];


	cout << "Max X: " << xMax << endl;
	cout << "Max Y: " << yMax << endl;
	cout << "Min X: " << xMin << endl;
	cout << "Min Y: " << yMin << endl;

	xOffset = int(xMin / cellSize);
	yOffset = int(yMin / cellSize);
	colCount = int(xMax / cellSize) - xOffset + 1;
	rowCount = int(yMax / cellSize) - yOffset + 1;

	totalLineCount = segmentsFromRegions[0].dx.size() + segmentsFromRegions[1].dx.size();

	thrust::host_vector<bool> gridCells;
	//gridCells.resize(rowCount * colCount * totalLineCount);

	gridCells.assign(rowCount * colCount * totalLineCount, false);
	cout << "Grid size: " << gridCells.size() << endl;

	return 0;
}

