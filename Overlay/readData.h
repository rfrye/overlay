#ifndef readData_h
#define readData_h

void readSegmentsFromFile(int originalIndex, string fileName, segments &segmentFromRegion);
void readSegmentsFromOdyssey(const unsigned int imap, string filename, segments &segmentsFromRegion);

#endif