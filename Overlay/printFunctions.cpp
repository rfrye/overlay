#include <iostream>
#include <fstream>
#include <unordered_set>
#include "segment.h"
#include "gridcell.h"
#include "indexedPoint.h"
#include "printFunctions.h"
using namespace std;
// All the print functions were used for debugging output
// All functions write to an output file
/*
void printCellsWithMultipleLineSegments(unordered_set<gridcell> & multipleLineSet)
{
	vector<segment> segList;
	int counter = 0;

	ofstream outputFile;
	outputFile.open("output_multiple_line_segments.txt");

	outputFile << "These cells contain multiple line segments\n";
	outputFile << "-------------------------------------------\n";

	for (gridcell cell : multipleLineSet)
	{
		outputFile << "Cell " << ++counter << "(" << cell.xCoord << "," << cell.yCoord << ")\n";
	}
	outputFile.close();
}

void printIndexPoints(vector<indexedPoint> & indexedPoints1, vector<indexedPoint> & indexedPoints2)
{
	ofstream outputFile;
	outputFile.open("output_index_points.txt");

	for (indexedPoint point : indexedPoints1)
		outputFile << "Point: (" << point.x << "," << point.y << ") Region: " << point.regionID << " Index " << point.index << "\n";

	for (indexedPoint point : indexedPoints2)
		outputFile << "Point: (" << point.x << "," << point.y << ") Region: " << point.regionID << " Index " << point.index << "\n";

	outputFile.close();
}

void printFinalSegs(vector<segment> & finalSegs1, vector<segment> & finalSegs2)
{
	ofstream outputFile;
	outputFile.open("output_final_segs.txt");
	//int counter = 0;

	outputFile << "Region 1" << endl;
	outputFile << "----------------------------" << endl;

	for (segment seg : finalSegs1)
		outputFile << "Line " << seg.segID << ": (" << seg.dx << "," << seg.dy << ") to (" << seg.sx << "," << seg.sy << ")\n";

	outputFile << "----------------------------" << endl;
	outputFile << "Region 2" << endl;
	outputFile << "----------------------------" << endl;
	//counter = 0;

	for (segment seg : finalSegs2)
		outputFile << "Line " << seg.segID << ": (" << seg.dx << "," << seg.dy << ") to (" << seg.sx << "," << seg.sy << ")\n";

	outputFile.close();
}
*/