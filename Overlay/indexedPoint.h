#include <thrust/device_vector.h>
#include <thrust/host_vector.h>

#ifndef INDEXEDPOINT_H
#define INDEXEDPOINT_H



struct indexedPoints {
	thrust::device_vector<int> h_segID;
	thrust::device_vector<int> h_regionID;
	thrust::device_vector<double> h_x;
	thrust::device_vector<double> h_y;
};

#endif