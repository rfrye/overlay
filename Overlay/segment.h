#include <iostream>
#include <thrust/device_vector.h>
#include <thrust/host_vector.h>
using namespace std;

#ifndef segment_h
#define segment_h

struct segments{
	// the left most end point is the ''dominating'' end point.
	// ALWAYS make sure the left most point is in the (dx,dy).
	//
	// There are lots of functions that depend on this assumption
	thrust::host_vector<double> dx;
	thrust::host_vector<double> dy;
	thrust::host_vector<double> sx;
	thrust::host_vector<double> sy;
	thrust::host_vector<double> regionID;
	thrust::host_vector<double> segID;
};

/*
struct segmentHasher
{
    std::size_t operator()(const segment& s) const
    {
        using std::size_t;
        using std::hash;
        size_t seed = 0;
        seed ^= std::hash<double>()(s.dx) + 0x9e3779b9 + (seed<<6) + (seed>>2);
        seed ^= std::hash<double>()(s.dy) + 0x9e3779b9 + (seed<<6) + (seed>>2);
        seed ^= std::hash<double>()(s.sx) + 0x9e3779b9 + (seed<<6) + (seed>>2);
        seed ^= std::hash<double>()(s.sy) + 0x9e3779b9 + (seed<<6) + (seed>>2);
        return seed;
    }
};
*/

#endif

