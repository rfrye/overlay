#include <omp.h>
#include <vector>
#include <algorithm>
#include <unordered_map>
#include <unordered_set>
#include <iostream>
#include "segment.h"
#include "indexedPoint.h"
#include "gridcell.h"
#include "breakSegments.h"
using namespace std;

/*
bool inCell(indexedPoint point, gridcell cell, int cellSize)
{
	int minX = cell.xCoord * cellSize;
	int maxX = minX + cellSize;
	int minY = cell.yCoord * cellSize;
	int maxY = minY + cellSize;

	if (point.x >= minX && point.x < maxX &&
		point.y >= minY && point.y < maxY)
		return true;
	else
		return false;
}


void findIntersectionPts(unordered_set<gridcell> multipleLineSet, vector<vector<vector<segment>>> storedSegments,
	vector<indexedPoint> result1, vector<indexedPoint> result2)
{
	int listSize;
	int cellSize = 0;
	indexedPoint p1, p2;

	for (gridcell cell : multipleLineSet)
	{
		vector<segment> segList = storedSegments[cell.xCoord][cell.yCoord];
		//cout << "Working Cell: (" << cell.xCoord << "," << cell.yCoord <<")" << endl;
		listSize = segList.size();
		//cout << "The list size: " << listSize << endl;
		// Compare every value in the list of segments, comparing each one against another.
		// Not sure if better way than nexted for loops
		for (int i = 0; i < listSize; i++)
		{
			for (int j = i + 1; j < listSize; j++)
			{
				// Need to change this to accept points from region 1 and region 2
				//if(segList[i].origIndex == 1 && segList[j].origIndex == 2)
				//{
				if (breakSegs(segList[i], segList[j], p1, p2))
				{
					//if (p1.regionID == 3 || p2.regionID == 3)
					//    cout << "Hey: found a regionID of 3!" << endl;
					// Not sure how regionID could be 3; so commenting out this section
					// and replacing with new section

					//if( p1.regionID  == 1 || p1.regionID == 3 ) result1.push_back( p1 );
					//if( p1.regionID == 2 || p1.regionID == 3 ) result2.push_back( p1 );
					//if( p2.regionID  == 1 || p2.regionID == 3 ) result1.push_back( p2 );
					//if( p2.regionID == 2 || p2.regionID == 3 ) result2.push_back( p2 );
					//cout << "P1: (" << p1.x << "," << p1.y << ") - Region: " << p1.regionID << " - Segment: " << p1.index << endl;
					//cout << "P2: (" << p2.x << "," << p2.y << ") - Region: " << p2.regionID << " - Segment: " << p2.index << endl;
					if (inCell(p1, cell, cellSize))
					{


						if (p1.regionID == 1)
						{
							result1.push_back(p1);
							//cout << "Added P1: " << p1.x << ", " << p1.y << " with index: " << p1.index << " for Region 1 and found in Cell " << cell.xCoord << ", " << cell.yCoord << endl;
						}
						if (p1.regionID == 2)
						{
							result2.push_back(p1);
							//cout << "Added P1: " << p1.x << ", " << p1.y << " with index: " << p2.index << " for Region 2 and found in Cell " << cell.xCoord << ", " << cell.yCoord << endl;
						}
					}
					else
					{
						//cout << "Did NOT add P1: " << p1.x << ", " << p1.y << " with index: " << p1.index << " for any Region and not found in Cell " << cell.xCoord << ", " << cell.yCoord << endl;
					}
					if (inCell(p2, cell, cellSize))
					{
						if (p2.regionID == 1)
						{
							result1.push_back(p2);
							//cout << "Added P2: " << p2.x << ", " << p2.y << " with index: " << p1.index << " for Region 1 and found in Cell " << cell.xCoord << ", " << cell.yCoord << endl;
						}
						if (p2.regionID == 2)
						{
							result2.push_back(p2);
							//cout << "Added P2: " << p2.x << ", " << p2.y << " with index: " << p2.index << " for Region 2 and found in Cell " << cell.xCoord << ", " << cell.yCoord << endl;
						}
					}
					else
					{
						//cout << "Did NOT add P2: " << p2.x << ", " << p2.y << " with index: " << p2.index << " for any Region and not found in Cell " << cell.xCoord << ", " << cell.yCoord << endl;
					}

					//  }
				}
			}
		}
	}

}

/***
* breakSegs takes 2 halfsegments and computes thier intersection points.
* there are 2 types of intersections: 1)  hsegs that intersect at a point, and 2) hsegs that intersect along a line.  The 2 points that are passed in will be set with the intersection points.
*
* Returns ``true`` if an intersection occurs
*
* newP and newQ are initialized to having a regionID of 0.  If the point is used, it will get the regionID of the region containing the segment that will be broken at that point.  We always assume h1 comes from region 1 and h2 comes from region 2
*
*/
/*
bool breakSegs(const segment & s1, const  segment & s2, indexedPoint & newP, indexedPoint &newQ)
{
	// newP and newQ are initialized to regionID 0.  If
	// an intersection point is found, it will be assigned the regionID of the
	// region to which it belongs
	newP.regionID = 0;
	newQ.regionID = 0;
	double x, y;
	//ifdef DEBUG_PRINT
	//pragma omp critical
	//    {
	//        cerr <<  s1.dx << " " << s1.dy << " " << s1.sx << " " << s1.sy << " s1" << endl;
	//        cerr <<  s2.dx << " " << s2.dy << " " << s2.sx << " " << s2.sy << " s2" << endl;
	//    }
	//endif
	// if colinear, get all the appropriate point
	if (s1.collinear(s2)) {
		double dx1 = s1.dx;
		double sx1 = s1.sx;
		double dx2 = s2.dx;
		double sx2 = s2.sx;
		// if the lines are closer to being vertical than horizontal,
		// use the y vals for colinearity tests
		// based on slope formula
		// need to change to the commented out statement for better
		// =handling of floating point errors.
		// but it will cause other changes in the code below.
		// for now, go with the simple test.
		// if( abs(s1.dy - s1.sy) > abs(s1.dx - s1.sx) )
		if (dx1 == sx1) {
			dx1 = s1.dy;
			sx1 = s1.sy;
			dx2 = s2.dy;
			sx2 = s2.sy;
		}
		// build up the cases
		// s1.dx is in h2
		if (dx2 < dx1 && dx1 < sx2) {
			// break s2 at dx1,dy1
			newP.regionID = s2.regionID;
			newP.index = s2.segID;
			newP.x = s1.dx;
			newP.y = s1.dy;
		}
		// s2.dx is in h1
		else if (dx1 < dx2 && dx2 < sx1) {
			// break s1 at dx2,dy2
			newP.regionID = s1.regionID;
			newP.index = s1.segID;
			newP.x = s2.dx;
			newP.y = s2.dy;
			// cerr << "2"<<endl;
		}
		// repeat for the segment end points
		// s1.sx is in s2
		if (dx2 < sx1 && sx1 < sx2) {
			// break s2 at sx1,sy1
			newQ.regionID = s2.regionID;
			newQ.index = s2.segID;
			newQ.x = s1.sx;
			newQ.y = s1.sy;
		}
		// s2.sx is in s1
		else if (dx1 < sx2 && sx2 < sx1) {
			// break s1 at sx2,sy2
			newQ.regionID = s1.regionID;
			newQ.index = s1.segID;
			newQ.x = s2.sx;
			newQ.y = s2.sy;
		}
		return newP.regionID != 0 || newQ.regionID != 0;
	}

	// if we get here, the segs are NOT colinear.
	// if they share an end point, there is nothing further to do.
	if ((s1.dx == s2.dx && s1.dy == s2.dy) ||
		(s1.sx == s2.sx && s1.sy == s2.sy) ||
		(s1.dx == s2.sx && s1.dy == s2.sy) ||
		(s1.sx == s2.dx && s1.sy == s2.dy)) {
		return false;
	}

	// now, we might have an intersection in segment interiors.
	// Lets find out
	// find intersection point
	double ax = s1.dx;
	double ay = s1.dy;
	double bx = s1.sx;
	double by = s1.sy;
	double cx = s2.dx;
	double cy = s2.dy;
	double dx = s2.sx;
	double dy = s2.sy;

	double denom = ((bx - ax)*(dy - cy) - (by - ay)*(dx - cx));
	double r = ((ay - cy)*(dx - cx) - (ax - cx)*(dy - cy)) / denom;
	double s = ((ay - cy)*(bx - ax) - (ax - cx)*(by - ay)) / denom;

	// if r and s are between 0 and 1 inclusive, we have an intersection
	// in both seg interiors
	double X, Y;
	if (0.0 <= r && r <= 1.0 && 0.0 <= s && s <= 1.0) {
		X = ax + (r*(bx - ax));
		Y = ay + (r*(by - ay));
		newP.regionID = s1.regionID;
		newP.index = s1.segID;
		newP.x = X;
		newP.y = Y;
		newQ.regionID = s2.regionID;
		newQ.index = s2.segID;
		newQ.x = X;
		newQ.y = Y;
		return true;
	}
	/*
	// next check to see if the intersection involves an endpoint and interior
	else if (  (0.0 == ua || 1.0 == ua)  && 0.0 < ub && ub < 1.0 ) {
	// h1's end point (left or right end point) and h2's interior
	X = x1;
	Y = y1;
	if( ua == 1.0){
	X = x2;
	Y = y2;
	}
	newP.regionID = 2;
	newP.index = s2.regionID;
	newP.x = X;
	newP.y = Y;
	return true;
	}
	else if (  (0.0 == ub || 1.0 == ub ) && 0.0 < ua && ua < 1.0 ) {
	// h2's end point (left or right) and h1's interior
	X = x3;
	Y = y3;
	if( ub == 1.0 ){
	X = x4;
	Y = y4;
	}
	newP.regionID = 1;
	newP.index = s1.regionID;
	newP.x = X;
	newP.y = Y;
	return true;
	}
	
	else {
		return false;
	}
	return true;
}


void breakSegsAtIntersectionPoints(vector <segment> &r1, vector <indexedPoint> &allPois, vector <segment> & finalSegs)
{
	segment newPiece;

	if (allPois.size() <= 0) {
		finalSegs = r1;
		return;
	}

	// sort them
	std::sort(allPois.begin(), allPois.end());
	cout << "Number of segs in Region: " << r1.size() << endl;
	cout << "Number of points: " << allPois.size() << endl;
	// now they are sorted by index, then by x, then by y
	int currPoi = 0;
	for (int i = 0; i < r1.size(); i++) {
		// ignore duplicate points
		while (currPoi < allPois.size() - 1
			&& allPois[currPoi].index == allPois[currPoi + 1].index
			&& allPois[currPoi].x == allPois[currPoi + 1].x
			&& allPois[currPoi].y == allPois[currPoi + 1].y)
		{
			//cout << "Ignoring duplicate point: (" << allPois[currPoi].x << "," << allPois[currPoi].y << ") from Segment: " << allPois[currPoi].index << endl;
			currPoi++;
		}
		// if the point's seg index matches this one, we need to break it
		// check if the seg is unbroken
		if (r1[i].segID < allPois[currPoi].index) {
			//cout << "Unbroken segment (" << r1[i].dx << "," << r1[i].dy << ")-(" << r1[i].sx << "," << r1[i].sy << ")" << endl;
			finalSegs.push_back(r1[i]);
			continue;
		}
		// if we get here, the seg index and currpoi index match
		// break the seg.
		newPiece.dx = r1[i].dx;
		newPiece.dy = r1[i].dy;
		newPiece.sx = allPois[currPoi].x;
		newPiece.sy = allPois[currPoi].y;
		newPiece.regionID = r1[i].regionID;
		newPiece.segID = r1[i].segID;
		finalSegs.push_back(newPiece);
		while (currPoi < allPois.size() - 1 && allPois[currPoi].index == allPois[currPoi + 1].index) {
			currPoi++;
			while (currPoi < allPois.size() - 1
				&& allPois[currPoi].index == allPois[currPoi + 1].index
				&& allPois[currPoi].x == allPois[currPoi + 1].x
				&& allPois[currPoi].y == allPois[currPoi + 1].y) currPoi++;

			newPiece.dx = newPiece.sx;
			newPiece.dy = newPiece.sy;
			newPiece.sx = allPois[currPoi].x;
			newPiece.sy = allPois[currPoi].y;
			newPiece.regionID = r1[i].regionID;
			newPiece.segID = r1[i].segID;
			finalSegs.push_back(newPiece);
		}
		newPiece.dx = newPiece.sx;
		newPiece.dy = newPiece.sy;
		newPiece.sx = r1[i].sx;
		newPiece.sy = r1[i].sy;
		newPiece.regionID = r1[i].regionID;
		newPiece.segID = r1[i].segID;
		finalSegs.push_back(newPiece);
		currPoi++;
	}
}
*/