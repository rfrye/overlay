#ifndef gridcell_h
#define gridcell_h

/*
 *  gridcell
 *
 *  This struct represents each cell in the grid that each segment may lie in.
 */

struct gridcell
{
    int xCoord;
    int yCoord;
    
    bool operator == (const gridcell & other) const {
        return (xCoord == other.xCoord && yCoord == other.yCoord);
    }
    
    bool operator < (const gridcell & other) const {
        return xCoord < other.xCoord || (xCoord == other.xCoord && yCoord < other.yCoord);
    }
};

// Used http://en.cppreference.com/w/cpp/utility/hash
// This provides the hash function for the gridcell struct
namespace std {
    template<>
    struct hash<gridcell>
    {
        size_t operator()(const gridcell& cell) const
        {
            using std::size_t;
            using std::hash;
            size_t seed = 0;
            seed ^= std::hash<double>()(cell.xCoord) + 0x9e3779b9 + (seed<<6) + (seed>>2);
            seed ^= std::hash<double>()(cell.yCoord) + 0x9e3779b9 + (seed<<6) + (seed>>2);
            return seed;
        }
    };
}




#endif