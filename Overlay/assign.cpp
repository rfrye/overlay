#include "assignCell.h"
using namespace std;


/*
*  Inserts the segment into the 3D-Vector with the grid as the key
*  Also check to see if the current grid cell contains multiple line segments,
*  if it does, then it adds it to the set of gridcells that contain multiple segments.
*/

__device__ void insertLineIntoList(int x, int y, segment seg,
	vector<vector<vector<segment>>> & vector3D,
	unordered_set<gridcell> & multipleLineSet)
{
	gridcell cell;
	cell.xCoord = x;
	cell.yCoord = y;

	vector<segment> *entry = &vector3D[x][y];
	entry->push_back(seg);
	
	//cout << "(" << seg.dx << "," << seg.dy << ") - (" << seg.sx << "," << seg.sy << ") assigned to cell: " << x << "," <<y << endl;
	
	if (entry->size() > 1)
		multipleLineSet.insert(cell);
}


/*
*  Bresenham's algorithm
*
*  translated to CPU C++ by Gage Hugo from Roger Frye's CUDA implementation
*
*  Updated to Xiaolin Wu's Line Algorithm which is a modified version of Brenenham's
*  Line Algorithm by Roger Frye
*/

void assignCell2(
	int *segIDs,
	int *regionIDs,
	double *dx,
	double *dy,
	double *sx,
	double *sy,
	bool *gridCells,
	int totalLineCount,
	int cellSize,
	int xOffset,
	int yOffset,
	int gridXSize,
	int gridYSize)
{
	// Get thread ID
	int threadID = 0;
		
	//Use thread ID to get segment data

	double x0 = dx[threadID];
	double y0 = dy[threadID];
	double x1 = sx[threadID];
	double y1 = sy[threadID];

	float error, slope;
	int currentX, currentY;
	int startGridX, endGridX, startGridY, endGridY;

	bool steep = abs(y1 - y0) > abs(x1 - x0);

	if (steep)
	{
		swap(x0, y0);
		swap(x1, y1);
	}
	if (x0 > x1)    // Should never happen as segements are order enforced
	{
		swap(x0, x1);
		swap(y0, y1);
	}
	if (x1 == x0)
		slope = 0;
	else
		slope = (y1 - y0) / (x1 - x0);


	startGridX = (int)(x0 / cellSize) - xOffset;
	endGridX = (int)(x1 / cellSize) - xOffset;
	startGridY = (int)(y0 / cellSize) - yOffset;
	endGridY = (int)(y1 / cellSize) - yOffset;
	
	//cout << "------- Start cellAssign2 ------- ";
	//if (steep)
	//cout << " ==== Steep ===" << endl;
	//else
	//cout << endl;
	
	// handle endpoints
	// If Dominate point is on grid boundary
	// and line is moving d->s,
	if ((fmod(y0, cellSize) == 0) && (y0 > y1))
	{
		if (steep)
		{
			//insertLineIntoList(startGridY, startGridX, seg, vector3D, multipleLineSet);
			int position = startGridX, startGridX
		}
		else
		{
			insertLineIntoList(startGridX, startGridY, seg, vector3D, multipleLineSet);
		}
		startGridY--;
	}
	// If subordinate point is on grid boundary
	// and line is moving d->s add cell on right
	// of boundary and reduce endGridX
	if (fmod(x1, cellSize) == 0)
	{
		if (steep)
		{
			insertLineIntoList(endGridY, endGridX, seg, vector3D, multipleLineSet);
		}
		else
		{
			insertLineIntoList(endGridX, endGridY, seg, vector3D, multipleLineSet);
		}
		endGridX--;
		// If subordinate point on grid intersection,
		// and line is moving d->s
		
		//cout << "Checking for sub point on grid intersection: " << endl <<
		//"\t Sub Point: " << x1 << ", " << y1 << endl <<
		//"\t Dom Point: " << x0 << ", " << y0 << endl <<
		//"\t endGridy: " << endGridY << endl;
		
		if (fmod(y1, cellSize) == 0 && y0 < y1)
		{
			endGridY--;
			
			//cout << "Adjusted: " << x1 << ", " << y1 << "  endGridY: " << endGridY <<  " endGridX: " << endGridX << endl;
			
		}
	}

	if (y0 == y1)
		error = 0;
	else
		error = (y0 - ((startGridY * cellSize) + (cellSize / 2))) / cellSize;
	//error = -1 * (((startGridY * cellSize) + (cellSize / 2) - y0) / (y1 - y0));
	
	//cout << "Error: " << error << " for seg: " << x0 << ", " << y0 << " - " << x1 << ", " << y1 << endl;
	
	if (x0 == x1)
		slope = 0;
	else
		slope = (y1 - y0) / (x1 - x0);

	// main loop
	currentY = startGridY;
	for (int currentX = startGridX; currentX < endGridX; currentX++)
	{
		if (steep)
		{
			insertLineIntoList(currentY, currentX, seg, vector3D, multipleLineSet);
			// Determine if the additional cell should be added
			if (slope < 0 && error < 0)
			{
				//                cout << "Following add is EXTRA cell" << endl;
				insertLineIntoList(currentY - 1, currentX, seg, vector3D, multipleLineSet);
			}
			else if (slope > 0 && error > 0)
			{
				//                cout << "Following add is EXTRA cell" << endl;
				insertLineIntoList(currentY + 1, currentX, seg, vector3D, multipleLineSet);
			}

		}
		else
		{
			insertLineIntoList(currentX, currentY, seg, vector3D, multipleLineSet);
			// Determine if the additional cell should be added
			if (slope > 0 && error > 0)
			{
				//                cout << "Following add is EXTRA cell" << endl;
				insertLineIntoList(currentX, currentY + 1, seg, vector3D, multipleLineSet);
			}
			else if (slope < 0 && error < 0)
			{
				//                cout << "Following add is EXTRA cell" << endl;
				insertLineIntoList(currentX, currentY - 1, seg, vector3D, multipleLineSet);
			}
		}

		error += slope;
		if (slope > 0 && error > 0.5)
		{
			
			//cout << "Addjust Y up - error: " << error << " slope: " << slope << " Cell: " << currentX << ", " << currentY << endl;
			
			error--;
			currentY++;
		}
		else if (slope < 0 && error < -0.5)
		{
			error++;
			currentY--;
		}
	}

	// Handle last cell separately
	if (steep)
	{
		insertLineIntoList(currentY, endGridX, seg, vector3D, multipleLineSet);
	}
	else
	{
		insertLineIntoList(endGridX, currentY, seg, vector3D, multipleLineSet);
	}

	//    cout << "------- End cellAssign2 ------- " << endl;

}

/*
__global__ void assignCell2(NUM_TYPE d_segDx[], NUM_TYPE d_segDy[],
							NUM_TYPE d_segSx[], NUM_TYPE s_segDy[],
							NUM_TYPE d_segRegionID[], NUM_TYPE d_segSegmentID[],
							int d_gridCellX[], int d_gridCellY[],
							const int cellSize)
{
	int index = threadIdx.x + (blockIdx.x * blockDim.x);

	double x0 = d_segDx[index];
	double y0 = d_segDy[index];
	double x1 = d_segSx[index];
	double y1 = d_segSy]index];

	float error, slope;
	int currentX, currentY;
	int startGridX, endGridX, startGridY, endGridY;

	bool steep = abs(y1 - y0) > abs(x1 - x0);

	if (steep)
	{
		swap(x0, y0);
		swap(x1, y1);
	}
	// Should never happen as segements are enforced ordered
	//if (x0 > x1)    
	//{
	//	swap(x0, x1);
	//	swap(y0, y1);
	//}
	if (x1 == x0)
		slope = 0;
	else
		slope = (y1 - y0) / (x1 - x0);


	startGridX = (int)x0 / cellSize;
	endGridX = (int)x1 / cellSize;
	startGridY = (int)y0 / cellSize;
	endGridY = (int)y1 / cellSize;
	/*
	cout << "------- Start cellAssign2 ------- ";
	if (steep)
	cout << " ==== Steep ===" << endl;
	else
	cout << endl;
	
	// handle endpoints
	// If Dominate point is on grid boundary
	// and line is moving d->s,
	if ((fmod(y0, cellSize) == 0) && (y0 > y1))
	{
		if (steep)
		{
			insertLineIntoList(startGridY, startGridX, seg, vector3D, multipleLineSet);
		}
		else
		{
			insertLineIntoList(startGridX, startGridY, seg, vector3D, multipleLineSet);
		}
		startGridY--;
	}


}
*/
