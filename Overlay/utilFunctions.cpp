#include "utilFunctions.h"
using namespace std;

int leftHandTurn(const double x1, const double y1,
	const double x2, const double y2,
	const double x3, const double y3)
{
	return ((y3 - y1) * (x2 - x1)) - ((y2 - y1) * (x3 - x1)) > 0;
	// result > 0 == left turn
	// result == 0 == colinear
	// result < 0 == right turn
}