#include <fstream>
#include <string>
#include <vector>
#include <unordered_map>
#include <unordered_set>
#include <thrust/device_vector.h>
#include <thrust/host_vector.h>
#include "segment.h"
#include "gridcell.h"
#include "readData.h"
#include <array>
using namespace std;



const std::string boldblacktty("\033[1;30m");   // tell tty to switch to bold black
const std::string redtty("\033[1;31m");   // tell tty to switch to bold red
const std::string greentty("\033[1;32m");   // tell tty to switch to bright green
const std::string bluetty("\033[34m");   // tell tty to switch to blue
const std::string magentatty("\033[1;35m");   // tell tty to switch to bright magenta
const std::string yellowbgtty("\033[1;43m");   // tell tty to switch to bright yellow background
const std::string underlinetty("\033[4m");   // tell tty to switch to underline
const std::string deftty("\033[0m");      // tell tty to switch back to default color

#define PRINT(arg)  #arg ": " <<(arg)     // Print an expression's name then its value, possibly
										  // followed by a comma or std::endl.
										  // Ex: std::cout << PRINTC(x) << PRINTN(y);
#define PRINTC(arg)  #arg << ": " << bluetty << (arg) << deftty << ", "
#define PRINTN(arg)  #arg << ": " << bluetty << (arg) << deftty << std::endl

										  // Assuming that the expression is a qualified name, e.g., a.b, print the name after the dot etc.
#define PRINTT(arg)  (index(#arg,'.')+1) << "=" << (arg)
#define PRINTCT(arg)  (index(#arg,'.')+1) << "=" << (arg) << ", "



void myerror(string msg) {
	cerr << "Error: " << msg << endl;
	exit(1);
}


// Splits the string by the given delimiter
void split(vector<string> & theStringVector, const string & theString, const string & theDelimiters)
{
	size_t  start = 0, end = 0;
	while (end != string::npos)
	{
		end = theString.find_first_of(theDelimiters, start);
		// If at end, use length=maxLength.  Else use length=end-start.
		theStringVector.push_back(theString.substr(start,
			(end == string::npos) ? string::npos : end - start));
		start = end;
		start = theString.find_first_not_of(theDelimiters, end);
	}
}

/*
*  Reads in the files and creates segments.
*/

void readSegmentsFromFile(int originalIndex, string fileName, segments &segmentsFromRegion)
{
	int segID = 0;
	double dx, dy, sx, sy;
	vector<string> splitLine;
	string line;
	ifstream readFile(fileName);
	while (getline(readFile, line))
	{
		if (line.size() == 0 || line[0] == '#') {
			cout << "Skipped";
			continue;
		}

		splitLine.clear();
		string delim(" ");
		split(splitLine, line, delim);

		segmentsFromRegion.segID.push_back(segID++);
		segmentsFromRegion.regionID.push_back(originalIndex + 1);

		dx = stod(splitLine[0]);
		dy = stod(splitLine[1]);
		sx = stod(splitLine[2]);
		sy = stod(splitLine[3]);

		if (dx < sx || (dx == sx && dy < sy))
		{
			segmentsFromRegion.dx.push_back(dx);
			segmentsFromRegion.dy.push_back(dy);
			segmentsFromRegion.sx.push_back(sx);
			segmentsFromRegion.sy.push_back(sy);
		}
		else
		{
			segmentsFromRegion.dx.push_back(sx);
			segmentsFromRegion.dy.push_back(sy);
			segmentsFromRegion.sx.push_back(dx);
			segmentsFromRegion.sy.push_back(dy);
		}
		//currentSegment.enforceOrdering();
	}
	readFile.close();
}


void readSegmentsFromOdyssey(const unsigned int imap, string filename, segments &segmentsFromRegion) {
	//char **args, const unsigned int imap) {   // and calc bounds
	unsigned int nv;       // Number of vertices in current polygon chain
	int     chainid, chainnode0, chainnode1;

	int lPolygon;
	int rPolygon;
	int segID = 0;
	double dx, dy, sx, sy;
	const unsigned int MAX_PTS_PER_CHAIN = 30000;   // Purely to catch probable input errors.
	const unsigned int MAX_GRID_HISTOGRAM = 1000;   // Size of histogram table for # edges per cell

	// ifstream fin(args[imap+1])
	cerr << filename.c_str() << endl;
	FILE *f = fopen(filename.c_str(), "r");
	if (f == NULL) {
		cerr << "ERROR: failed to open file " << filename << ", " << PRINTN(imap);
		exit(1);
	}
	cout << "Reading map " << imap << ", file " << filename << endl;
	while (fscanf(f, "%d %d %d %d %d %d", &chainid, &nv, &chainnode0, &chainnode1, &lPolygon, &rPolygon) == 6) {
		// various criteria for determining the end of the file
		//if (nv <= 0 || nv == 999 || nv == 9999) break;
		if (nv >= MAX_PTS_PER_CHAIN) {  // Probable input error
			cerr << PRINTC(imap) << PRINTN(nv);
			myerror("Too many points in this chain.");
		}
		if (lPolygon == rPolygon)
			cout << "WARNING: In map #" << imap 
			<< " has both left and right polygons the same = " << lPolygon
			<< ".  We don't handle this case completely yet." << endl;
		if (fscanf(f, "%lf %lf", &dx, &dy) != 2) {
			perror("unexpectedly while reading chain");
			fclose(f);
			exit(1);
		};
		for (unsigned int iv = 1; iv < nv; iv++) {          // Read in the coordinates of the vertices
			// Coordinates in data file may use exponential notation.
			if (fscanf(f, "%lf %lf", &sx, &sy) != 2) {
				perror("unexpectedly while reading chain");
				fclose(f);
				exit(1);
			};

			segmentsFromRegion.segID.push_back(segID++);
			segmentsFromRegion.regionID.push_back(imap+1);
			if (dx < sx || (dx == sx && dy < sy))
			{
				segmentsFromRegion.dx.push_back(dx);
				segmentsFromRegion.dy.push_back(dy);
				segmentsFromRegion.sx.push_back(sx);
				segmentsFromRegion.sy.push_back(sy);
			}
			else
			{
				segmentsFromRegion.dx.push_back(sx);
				segmentsFromRegion.dy.push_back(sy);
				segmentsFromRegion.sx.push_back(dx);
				segmentsFromRegion.sy.push_back(dy);
			}
			dx = sx;
			dy = sy;
		}
	}
	fclose(f);
}